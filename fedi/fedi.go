package fedi

import (
	"bridge-youtube/types"
	"bytes"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"time"

	"github.com/go-resty/resty/v2"
	"github.com/pkg/errors"
)

type Client struct {
	addr        string
	userID      string
	secret      string
	restyClient *resty.Client
}

func Make(addr, userID, secret string) *Client {
	client := resty.New().SetBaseURL(addr).SetHeaders(map[string]string{
		"Content-Type": "application/json",
		"Accept":       "application/json",
	}).SetTimeout(time.Second * 3).SetDisableWarn(true)

	return &Client{
		addr:        addr,
		userID:      userID,
		secret:      secret,
		restyClient: client,
	}
}

func (c *Client) GetAccessToken() (string, error) {
	var rspBody map[string]interface{}

	rsp, err := c.restyClient.R().SetBody(map[string]string{
		"client_id":     c.userID,
		"client_secret": c.secret,
		"redirect_uri":  "urn:ietf:wg:oauth:2.0:oob",
		"grant_type":    "client_credentials",
	}).SetResult(&rspBody).Post("oauth/token")

	if err := checkErr(rsp, err); err != nil {
		return "", errors.Wrap(err, "unable to get access token")
	}

	token, ok := rspBody["access_token"]
	if !ok {
		return "", errors.New("no token in response")
	}

	return token.(string), nil
}

func (c *Client) AccountVerifyCredentials(accessToken string) (map[string]interface{}, error) {
	var rspBody map[string]interface{}

	rsp, err := c.restyClient.R().SetAuthToken(accessToken).SetResult(&rspBody).
		Get("/api/v1/accounts/verify_credentials")

	if err := checkErr(rsp, err); err != nil {
		return nil, errors.Wrap(err, "unable to verify user creds")
	}

	return rspBody, nil
}

func (c *Client) CreateAccount(account *types.FediAccount) error {
	token, err := c.GetAccessToken()
	if err != nil {
		return errors.Wrap(err, "unable to get access token")
	}

	var rspBody map[string]interface{}

	rsp, err := c.restyClient.R().SetAuthToken(token).SetBody(map[string]interface{}{
		"reason":    "bridge-auto",
		"username":  account.Username,
		"email":     account.Email,
		"password":  account.Password,
		"agreement": true,
		"locale":    "en",
	}).SetResult(&rspBody).Post("api/v1/accounts")

	if err := checkErr(rsp, err); err != nil {
		return errors.Wrap(err, "unable to get access token")
	}

	userAccessToken, ok := rspBody["access_token"]
	if !ok {
		return errors.New("no token in response")
	}
	account.AccessToken = userAccessToken.(string)

	accountData, err := c.AccountVerifyCredentials(account.AccessToken)
	if err != nil {
		return errors.Wrap(err, "unable to verify account credentials")
	}

	accountID, ok := accountData["id"]
	if !ok {
		return errors.New("no id in response")
	}
	account.ID = accountID.(string)

	return nil
}

func (c *Client) AccountFollowers(account *types.FediAccount) ([]types.FediFollower, error) {
	var followers []types.FediFollower

	rsp, err := c.restyClient.R().SetBasicAuth(account.Email, account.Password).SetResult(&followers).
		Get(fmt.Sprintf("/api/v1/accounts/%s/followers", account.ID))
	if err := checkErr(rsp, err); err != nil {
		return nil, errors.Wrap(err, "unable to get account followers")
	}

	return followers, nil
}

func (c *Client) UploadMedia(user *types.FediAccount, data []byte) (string, error) {
	var rspData map[string]interface{}

	rsp, err := c.restyClient.R().SetBasicAuth(user.Email, user.Password).SetResult(&rspData).
		SetFileReader("file", "preview.jpg", bytes.NewReader(data)).Post("/api/v2/media")

	if err := checkErr(rsp, err); err != nil {
		return "", errors.Wrap(err, "unable to post media")
	}

	mediaID, ok := rspData["id"]
	if !ok {
		return "", errors.New("no media id in response")
	}

	return mediaID.(string), nil
}

func (c *Client) CreateStatus(user *types.FediAccount, status string, mediaID *string) (string, error) {
	var rspData map[string]interface{}

	rqBody := map[string]interface{}{
		"status": status,
	}
	if mediaID != nil {
		rqBody["media_ids"] = []string{*mediaID}
	}

	rsp, err := c.restyClient.R().SetBasicAuth(user.Email, user.Password).SetResult(&rspData).SetBody(rqBody).
		Post("/api/v1/statuses")

	if err := checkErr(rsp, err); err != nil {
		return "", errors.Wrap(err, "unknown err")
	}

	statusID, ok := rspData["id"]
	if !ok {
		return "", errors.New("no id in response data")
	}

	return statusID.(string), nil
}

func (c *Client) UpdateCredentials(account *types.FediAccount) error {
	if account.AccessToken == "" {
		return errors.New("account does not have access token")
	}

	locked, sensitive := "false", "false"
	if account.Locked {
		locked = "true"
	}

	if account.Sensitive {
		sensitive = "true"
	}

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)

	filePart, _ := writer.CreateFormFile("avatar", "avatar.jpg")
	io.Copy(filePart, bytes.NewReader(account.Avatar))

	if account.Banner != nil {
		headerPart, _ := writer.CreateFormFile("header", "header.jpg")
		io.Copy(headerPart, bytes.NewReader(account.Banner))
	}

	writer.WriteField("locked", locked)
	writer.WriteField("source[sensitive]", sensitive)
	writer.WriteField("display_name", account.DisplayName)
	writer.WriteField("source[note]", account.Description)
	writer.WriteField("note", account.Description)

	if account.IsBot {
		writer.WriteField("bot", "true")
	}

	writer.Close()

	r, _ := http.NewRequest("PATCH",
		fmt.Sprintf("%s/%s", c.restyClient.BaseURL, "api/v1/accounts/update_credentials"), body)
	r.Header.Add("Content-Type", writer.FormDataContentType())
	r.Header.Add("Accept", "application/json")
	r.Header.Add("Authorization", fmt.Sprintf("Bearer %s", account.AccessToken))

	rsp, err := c.restyClient.GetClient().Do(r)
	if err != nil {
		return errors.Wrap(err, "unable to update creds")
	}

	if rsp.StatusCode >= 300 {
		bts, _ := io.ReadAll(rsp.Body)
		return errors.Errorf("code: %d body: %s", rsp.StatusCode, string(bts))
	}

	return nil
}

func checkErr(rsp *resty.Response, err error) error {
	if err != nil {
		return err
	}

	if rsp.StatusCode() >= 300 {
		return errors.Errorf("code: %d body: %s", rsp.StatusCode(), rsp.String())
	}

	return nil
}
