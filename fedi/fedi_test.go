package fedi

import (
	"bridge-youtube/types"
	"fmt"
	"io"
	"os"
	"testing"
	"time"
)

func TestClient_CreateAccount(t *testing.T) {
	fp, err := os.Open("")
	if err != nil {
		t.Fatal(err)
	}

	bts, err := io.ReadAll(fp)
	if err != nil {
		t.Fatal(err)
	}

	cl := Make("http://localhost:8080", "",
		"")

	tm := time.Now().Unix()

	acc := types.FediAccount{
		Username:    fmt.Sprintf("test_%d", tm),
		Email:       fmt.Sprintf("test_%d@gmail.com", tm),
		Password:    "",
		Avatar:      bts,
		Locked:      false,
		Sensitive:   false,
		DisplayName: "zal",
	}

	err = cl.CreateAccount(&acc)

	if err != nil {
		t.Fatal(err)
	}

	if err := cl.UpdateCredentials(&acc); err != nil {
		t.Fatal(err)
	}
}
