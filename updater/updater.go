package updater

import (
	"bridge-youtube/db"
	"bridge-youtube/types"
	"fmt"
	"log"
	"time"

	"gorm.io/gorm"

	"github.com/pkg/errors"
)

type DBConnector interface {
	GetUser(username string) (*db.User, error)
	GetVideo(id string) (*db.Video, error)
	SaveVideo(v *db.Video) error
	UpdateUser(u *db.User) error
}

type YoutubeClient interface {
	GetChannelVideos(channelID string) ([]types.Video, error)
}

type Fedi interface {
	UploadMedia(user *types.FediAccount, data []byte) (string, error)
	CreateStatus(user *types.FediAccount, status string, mediaID *string) (string, error)
}

type Daemon struct {
	connector      DBConnector
	queue          types.Queue
	queueName      string
	yt             YoutubeClient
	fedi           Fedi
	updateInterval time.Duration
}

func Make(conn DBConnector, queue types.Queue, ytClient YoutubeClient, fedi Fedi, queueName string,
	updateInterval time.Duration) *Daemon {
	return &Daemon{
		connector:      conn,
		queue:          queue,
		queueName:      queueName,
		yt:             ytClient,
		fedi:           fedi,
		updateInterval: updateInterval,
	}
}

func (d *Daemon) Start() error {
	msgs, err := d.queue.Consume(d.queueName)
	if err != nil {
		return errors.Wrap(err, "unable to consume queue")
	}

	for m := range msgs {
		log.Printf("receive message: %s", m)

		if err := d.parseMessage(m.Body); err != nil {
			log.Println(err)
		}
	}

	return nil
}

func (d *Daemon) parseMessage(m string) error {
	u, err := d.connector.GetUser(m)
	if err != nil {
		return errors.Wrapf(err, "user [%s] not found", m)
	}

	if u.LastUpdate != nil && u.LastUpdate.Add(d.updateInterval).Unix() > time.Now().Unix() {
		log.Printf("skip parsing for account: %s, last update %s", m, u.LastUpdate.Format(time.RFC3339))
		return nil
	}

	videos, err := d.yt.GetChannelVideos(u.YtChannelID)
	if err != nil {
		return errors.Wrap(err, "unable to get channel videos")
	}

	//for _, v := range videos {
	for i := 0; i < len(videos); i++ {
		v := videos[len(videos)-1-i]

		dbVid, err := d.connector.GetVideo(v.ID)
		if err != nil && errors.Cause(err) != gorm.ErrRecordNotFound {
			return errors.Wrap(err, "unknown err")
		}
		if dbVid != nil {
			continue
		}

		image, err := types.DownloadImage(v.Thumbnail)
		if err != nil {
			return errors.Wrap(err, "unable to download image")
		}

		fediAccount := types.FediAccount{Email: u.Email, Password: u.Password}

		mediaID, err := d.fedi.UploadMedia(&fediAccount, image)
		if err != nil {
			return errors.Wrap(err, "unable to upload media")
		}

		statusID, err := d.fedi.CreateStatus(&fediAccount, fmt.Sprintf("%s\n%s", v.Title, v.URL()), &mediaID)
		if err != nil {
			return errors.Wrap(err, "unable to create status")
		}

		if err := d.connector.SaveVideo(&db.Video{
			Title:         v.Title,
			Thumbnail:     v.Thumbnail,
			ID:            v.ID,
			Description:   v.Description,
			PublishedAt:   v.PublishedAt,
			ChannelID:     u.YtChannelID,
			FediID:        statusID,
			ThumbnailSize: len(image),
		}); err != nil {
			return errors.Wrap(err, "unable to save video")
		}
	}

	now := time.Now()
	u.LastUpdate = &now

	if err := d.connector.UpdateUser(u); err != nil {
		return errors.Wrap(err, "unable to update user")
	}

	return nil
}
