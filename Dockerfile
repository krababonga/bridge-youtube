FROM --platform=${TARGETPLATFORM} alpine:3.15.4 as executor

WORKDIR /bridge
COPY --chown=1000:1000 bridge-youtube /bridge/bridge-youtube

ENTRYPOINT [ "/bridge/bridge-youtube" ]