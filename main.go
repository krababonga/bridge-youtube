package main

import (
	"bridge-youtube/cron"
	"bridge-youtube/db"
	"bridge-youtube/fedi"
	"bridge-youtube/listener"
	"bridge-youtube/queue"
	"bridge-youtube/updater"
	"bridge-youtube/yt"
	"context"
	"flag"
	"log"
	"time"

	"github.com/joho/godotenv"
	"github.com/kelseyhightower/envconfig"
)

var (
	job        string
	configPath string
)

const (
	JobListener = "listener"
	JobUpdater  = "updater"
	JobCron     = "cron"
)

func init() {
	flag.StringVar(&job, "j", JobListener, "app job")
	flag.StringVar(&configPath, "c", ".env", "config path")
	flag.Parse()
}

func main() {
	if err := run(); err != nil {
		log.Fatalln(err)
	}
}

func run() error {
	if err := godotenv.Load(configPath); err != nil {
		return err
	}

	var cnf Config

	err := envconfig.Process("", &cnf)
	if err != nil {
		return err
	}

	//youtubeService, err := yt.Make(cnf.GoogleAPIKey)
	//if err != nil {
	//	return err
	//}
	youtubeService := yt.MakeInvidious()

	fediClient := fedi.Make(cnf.FediAddr, cnf.FediAppUserID, cnf.FediAppSecret)

	dbConn, err := db.Make(cnf.DBType, cnf.DBDSN)
	if err != nil {
		return err
	}

	q, err := queue.Make(&queue.Config{RQUser: cnf.RabbitUser, RQPassword: cnf.RabbitPassword, RQHost: cnf.RabbitHost,
		RQPort: cnf.RabbitPort, ConsumerName: "yt-bridge"})
	if err != nil {
		return err
	}

	switch job {
	case JobListener:
		if err := jobListener(&cnf, youtubeService, fediClient, dbConn, q); err != nil {
			return err
		}
	case JobUpdater:
		if err := jobUpdater(&cnf, youtubeService, fediClient, dbConn, q); err != nil {
			return err
		}
	case JobCron:
		if err := jobCron(&cnf, fediClient, dbConn, q); err != nil {
			return err
		}

	}

	return nil
}

func jobUpdater(cnf *Config, youtubeService updater.YoutubeClient, fediClient *fedi.Client, dbConn *db.Connector,
	q *queue.RabbitMQ) error {

	log.Println("start updater")

	dur, err := time.ParseDuration(cnf.UserUpdateInterval)
	if err != nil {
		return err
	}

	upd := updater.Make(dbConn, q, youtubeService, fediClient, cnf.UpdaterQueueName, dur)
	if err := upd.Start(); err != nil {
		return err
	}

	return nil
}

func jobListener(cnf *Config, youtubeService listener.Youtube, fediClient *fedi.Client, dbConn *db.Connector,
	q *queue.RabbitMQ) error {

	log.Println("start listener")

	l := listener.Make(&listener.Config{
		RQPort:              cnf.RabbitPort,
		RQHost:              cnf.RabbitHost,
		RQPassword:          cnf.RabbitPassword,
		RQUser:              cnf.RabbitUser,
		AccountQueue:        cnf.AccountQueue,
		ResponseQueuePrefix: cnf.ResponseQueuePrefix,
		UpdaterQueue:        cnf.UpdaterQueueName,
	}, youtubeService, fediClient, dbConn, q)

	if err := l.Start(); err != nil {
		return err
	}

	return nil
}

func jobCron(cnf *Config, fediClient *fedi.Client, dbConn *db.Connector,
	q *queue.RabbitMQ) error {
	log.Println("start cron")
	c := cron.Make(dbConn, q, fediClient, cnf.UpdaterQueueName)
	if err := c.Do(context.Background()); err != nil {
		return err
	}

	return nil
}
