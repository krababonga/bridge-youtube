package listener

import (
	"bridge-youtube/db"
	"bridge-youtube/types"
	"context"
	"fmt"
	"log"
	"time"

	"gorm.io/gorm"

	"github.com/pkg/errors"
	"github.com/sethvargo/go-password/password"
)

const (
	NotFoundMessage = "NotFound"
)

type Config struct {
	RQHost              string
	RQPort              string
	RQUser              string
	RQPassword          string
	AccountQueue        string
	ResponseQueuePrefix string
	UpdaterQueue        string
}

type Youtube interface {
	SearchChannel(text string) (*types.Channel, error)
}

type Fedi interface {
	CreateAccount(account *types.FediAccount) error
	UpdateCredentials(account *types.FediAccount) error
}

type DBConnector interface {
	GetUser(username string) (*db.User, error)
	CreateUser(u *db.User) error
	GetUserBySearch(search string) (*db.User, error)
	SaveSearchResult(text string, userID uint) error
}

type Daemon struct {
	cnf   *Config
	yt    Youtube
	fd    Fedi
	db    DBConnector
	queue types.Queue
}

func Make(cnf *Config, yt Youtube, fd Fedi, db DBConnector, queue types.Queue) *Daemon {
	return &Daemon{
		cnf:   cnf,
		yt:    yt,
		fd:    fd,
		db:    db,
		queue: queue,
	}
}

func (d *Daemon) Start() error {
	msgs, err := d.queue.Consume(d.cnf.AccountQueue)
	if err != nil {
		return errors.Wrap(err, "unable to start consumer")
	}

	for m := range msgs {
		log.Printf("Received a message: %s", m.Body)

		//continue

		var responseMessage string
		accUsername, err := d.createAccount(m.Body)
		if err != nil {
			responseMessage = NotFoundMessage

			if !types.IsErrNotFound(err) {
				log.Printf("%+v", err)

				//go func() {
				//	time.Sleep(time.Second * 5)
				//
				//	if err := m.Child.Nack(false, true); err != nil {
				//		log.Println(err)
				//	}
				//}()
			}
		} else {
			//if err := m.Child.Ack(false); err != nil {
			//	log.Println(err)
			//}

			responseMessage = accUsername
		}

		if err := d.sendChannelResponse(m.Body, responseMessage); err != nil {
			log.Printf("%+v", err)
		}
	}

	return nil
}

func (d *Daemon) createAccount(message string) (string, error) {
	usr, err := d.db.GetUserBySearch(message)
	if err != nil && errors.Cause(err) != gorm.ErrRecordNotFound {
		return "", errors.Wrap(err, "unable ot check user") // fatal
	}

	if usr != nil {
		log.Println("return response from search cache")
		return usr.Username, nil
	}

	ch, err := d.yt.SearchChannel(message)
	if err != nil {
		return "", errors.Wrap(err, "unable to find channel")
	}

	image, err := types.DownloadImage(ch.Image)
	if err != nil {
		return "", errors.Wrap(err, "unable to download image")
	}

	pwd, err := password.Generate(20, 5, 5, false, false)
	if err != nil {
		return "", errors.Wrap(err, "unable to generate password")
	}

	acc := types.FediAccount{
		Username:    ch.CustomURL,
		DisplayName: ch.Title,
		Avatar:      image,
		Sensitive:   false,
		Locked:      false,
		Email:       fmt.Sprintf("%s@yt.fedi.local", ch.CustomURL),
		Password:    pwd,
		Description: ch.Description,
		IsBot:       true,
	}

	if ch.BannerURL != "" {
		banner, err := types.DownloadImage(ch.BannerURL)
		if err != nil {
			return "", errors.Wrap(err, "unable to download banner")
		}
		acc.Banner = banner
	}

	_, userErr := d.db.GetUser(acc.Username)
	if userErr == nil {
		return acc.Username, nil
	}
	if errors.Cause(userErr) != gorm.ErrRecordNotFound {
		return "", errors.Wrap(err, "unable to check user in db")
	}

	if err := d.fd.CreateAccount(&acc); err != nil {
		return "", errors.Wrap(err, "unable to create account")
	}

	if err := d.fd.UpdateCredentials(&acc); err != nil {
		return "", errors.Wrap(err, "unable to update user credentials")
	}

	newUser := db.User{
		Username:    acc.Username,
		Password:    acc.Password,
		Email:       acc.Email,
		AccessToken: acc.AccessToken,
		YtChannelID: ch.ID,
		Description: ch.Description,
		DisplayName: acc.DisplayName,
		FediID:      acc.ID,
	}

	if err := d.db.CreateUser(&newUser); err != nil {
		return "", errors.Wrap(err, "unable to save user to DB")
	}

	if err := d.db.SaveSearchResult(message, newUser.ID); err != nil {
		return "", errors.Wrap(err, "unable to save search result")
	}

	go func() {
		time.Sleep(time.Minute * 5)
		d.queue.Send(context.Background(), d.cnf.UpdaterQueue, newUser.Username)
	}()

	return acc.Username, nil
}

func (d *Daemon) sendChannelResponse(receivedMessage, toSend string) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	if err := d.queue.Send(ctx,
		fmt.Sprintf("%s%s", d.cnf.ResponseQueuePrefix, receivedMessage), toSend); err != nil {
		return errors.Wrap(err, "unable to send response")
	}

	return nil
}
