package main

type Config struct {
	GoogleAPIKey        string `envconfig:"GOOGLE_API_KEY"`
	RabbitHost          string `envconfig:"RABBIT_HOST"`
	RabbitPort          string `envconfig:"RABBIT_PORT" default:"5672"`
	RabbitUser          string `envconfig:"RABBIT_USER"`
	RabbitPassword      string `envconfig:"RABBIT_PASSWORD"`
	AccountQueue        string `envconfig:"ACCOUNT_QUEUE" default:"bridge-accounts"`
	ResponseQueuePrefix string `envconfig:"RESPONSE_QUEUE_PREFIX" default:"bridge-"`
	FediAddr            string `envconfig:"FEDI_ADDR"`
	FediAppUserID       string `envconfig:"FEDI_APP_USER_ID"`
	FediAppSecret       string `envconfig:"FEDI_APP_SECRET"`
	DBType              string `envconfig:"DB_TYPE" default:"sqlite"`
	DBDSN               string `envconfig:"DB_DSN" default:"yt_bridge.sqlite"`
	UserUpdateInterval  string `envconfig:"USER_UPDATE_INTERVAL" default:"30m"`
	UpdaterQueueName    string `envconfig:"UPDATER_QUEUE_NAME" default:"yt-bridge-updater"`
}
