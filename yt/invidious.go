package yt

import (
	"bridge-youtube/types"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/go-resty/resty/v2"
	"github.com/gosimple/slug"
	"github.com/pkg/errors"
)

type Invidious struct {
	r         *resty.Client
	instances chan string
	//allInstances []string
}

type SearchResult struct {
	Type             string      `json:"type"`
	Author           string      `json:"author"`
	AuthorID         string      `json:"authorId"`
	AuthorThumbnails []Thumbnail `json:"authorThumbnails"`
	Description      string      `json:"description"`
	VideoID          string      `json:"videoId"`
	Title            string      `json:"title"`
	VideoThumbnails  []Thumbnail `json:"videoThumbnails"`
	Published        int64       `json:"published"`
}

func (sr SearchResult) LargestAuthorURL() string {
	channelURL := ""
	largest := -1

	for _, thumb := range sr.AuthorThumbnails {
		if thumb.Width > largest {
			channelURL = thumb.URL
			largest = thumb.Width
		}
	}

	return appendHTTPs(channelURL)
}

func (sr SearchResult) CustomURL() string {
	customURL := slug.Make(sr.Author)
	return strings.Replace(customURL, "-", "_", -1)
}

func (sr SearchResult) PublishedTime() time.Time {
	return time.Unix(sr.Published, 0)
}

func (sr SearchResult) GetVideoThumbnail(quality string) string {
	var ret string
	for _, t := range sr.VideoThumbnails {
		if t.Quality == quality {
			ret = t.URL
			break
		}

		if t.Quality == "default" && ret == "" {
			ret = t.URL
		}
	}

	if ret == "" && len(sr.VideoThumbnails) > 0 {
		ret = sr.VideoThumbnails[0].URL
	}

	return appendHTTPs(ret)
}

func appendHTTPs(str string) string {
	if !strings.Contains(str, "http") {
		str = fmt.Sprintf("https:%s", str)
	}

	return str
}

type Thumbnail struct {
	Quality string `json:"quality"`
	URL     string `json:"url"`
	Width   int    `json:"width"`
	Height  int    `json:"height"`
}

func MakeInvidious() *Invidious {
	i := Invidious{
		r:         resty.New().SetTimeout(time.Second * 3).SetDisableWarn(true),
		instances: make(chan string, 5),
		//allInstances: make([]string, 0),
	}

	go i.instanceUpdater()

	return &i
}

func (i *Invidious) instanceUpdater() {
	for {
		if err := i.instancesUpdate(); err != nil {
			log.Println(err)
			time.Sleep(time.Second)
		}
	}
}

func (i *Invidious) instancesUpdate() error {
	all, err := i.loadInstances()
	if err != nil {
		return errors.Wrap(err, "unable to load instances")
	}

	for _, a := range all {
		var res map[string]interface{}
		rsp, err := i.r.R().SetResult(&res).Get(fmt.Sprintf("https://%s/api/v1/stats", a))
		if err := checkErr(rsp, err); err != nil {
			log.Printf("server %s is unavailable, err: %s", a, err)
			continue
		}
		if _, ok := res["version"]; !ok {
			continue
		}

		i.instances <- a
	}

	return nil
}

func (i *Invidious) loadInstances() ([]string, error) {
	var result []interface{}
	rsp, err := i.r.R().SetResult(&result).Get("https://api.invidious.io/instances.json?pretty=1&sort_by=type,users")
	if err := checkErr(rsp, err); err != nil {
		return nil, errors.Wrap(err, "unknown err")
	}

	var all []string
	for _, instance := range result {
		i := instance.([]interface{})
		addr := i[0].(string)
		params := i[1].(map[string]interface{})
		_type, ok := params["type"]
		if !ok {
			continue
		}
		api, ok := params["api"]
		if !ok {
			continue
		}

		if api != nil && api.(bool) && _type != nil && _type.(string) == "https" {
			all = append(all, addr)
		}
	}

	return all, nil
}

func (i *Invidious) SearchChannel(text string) (*types.Channel, error) {
	instance := <-i.instances
	var searchResult []SearchResult
	rsp, err := i.r.R().SetResult(&searchResult).SetQueryParams(map[string]string{
		"q": text,
	}).Get(fmt.Sprintf("https://%s/api/v1/search", instance))

	if err := checkErr(rsp, err); err != nil {
		return nil, errors.Wrap(err, "unable to search")
	}

	channelFromVideo := ""

	for _, sr := range searchResult {
		if sr.Type == "channel" {
			return &types.Channel{
				ID:          sr.AuthorID,
				Title:       sr.Author,
				Image:       sr.LargestAuthorURL(),
				CustomURL:   sr.CustomURL(),
				Description: sr.Description,
			}, nil
		} else if sr.Type == "video" {
			channelFromVideo = sr.AuthorID
		}
	}

	if channelFromVideo != "" {
		return i.GetChannel(channelFromVideo)
	}

	return nil, types.ErrNotFound("channel not found")
}

func (i *Invidious) GetChannel(channelID string) (*types.Channel, error) {
	instance := <-i.instances

	var sr SearchResult
	rsp, err := i.r.R().SetResult(&sr).Get(fmt.Sprintf("https://%s/api/v1/channels/%s", instance, channelID))
	if err := checkErr(rsp, err); err != nil {
		return nil, errors.Wrap(err, "unable to load channel")
	}

	customURL := slug.Make(sr.Author)
	customURL = strings.Replace(customURL, "-", "_", -1)

	return &types.Channel{
		ID:          sr.AuthorID,
		Title:       sr.Author,
		Image:       sr.LargestAuthorURL(),
		CustomURL:   sr.CustomURL(),
		Description: sr.Description,
	}, nil
}

func (i *Invidious) GetChannelVideos(channelID string) ([]types.Video, error) {
	instance := <-i.instances

	var searchResults []SearchResult
	rsp, err := i.r.R().SetResult(&searchResults).Get(
		fmt.Sprintf("https://%s/api/v1/channels/%s/latest", instance, channelID))

	if err := checkErr(rsp, err); err != nil {
		return nil, errors.Wrap(err, "unable to get videos")
	}

	var videos []types.Video
	for _, sr := range searchResults {
		videos = append(videos, types.Video{
			ID:          sr.VideoID,
			Title:       sr.Title,
			Description: sr.Description,
			Thumbnail:   sr.GetVideoThumbnail("high"),
			PublishedAt: sr.PublishedTime(),
		})
	}

	return videos, nil
}

func checkErr(rsp *resty.Response, err error) error {
	if err != nil {
		return err
	}

	if rsp.StatusCode() >= 300 {
		return errors.Errorf("code: %d body: %s", rsp.StatusCode(), rsp.String())
	}

	return nil
}
