package yt

import (
	"bridge-youtube/types"
	"context"
	"strings"
	"time"

	"github.com/gosimple/slug"

	"github.com/pkg/errors"
	"google.golang.org/api/option"
	"google.golang.org/api/youtube/v3"
)

type Service struct {
	apiToken string
	service  *youtube.Service
}

func Make(apiToken string) (*Service, error) {
	service, err := youtube.NewService(context.Background(), option.WithAPIKey(apiToken))
	if err != nil {
		return nil, errors.Wrap(err, "unable to init yt service")
	}

	return &Service{
		apiToken: apiToken,
		service:  service,
	}, nil
}

func (s *Service) SearchChannel(text string) (*types.Channel, error) {
	text = strings.ToLower(text)

	call := s.service.Search.List([]string{"id", "snippet"}).
		Q(text).
		MaxResults(5)
	response, err := call.Do()
	if err != nil {
		return nil, errors.Wrap(err, "unable to get query response")
	}

	for _, item := range response.Items {
		if item.Id.Kind == "youtube#channel" {
			channel, err := s.loadChannel(item.Id.ChannelId)
			if err != nil {
				return nil, errors.Wrap(err, "unable to load channel")
			}

			return channel, nil
		}
	}

	if len(response.Items) > 0 && response.Items[0].Snippet.ChannelId != "" {
		channel, err := s.loadChannel(response.Items[0].Snippet.ChannelId)
		if err != nil {
			return nil, errors.Wrap(err, "unable to load channel")
		}

		return channel, nil
	}

	return nil, types.ErrNotFound("channel not found")
}

func (s *Service) loadChannel(id string) (*types.Channel, error) {
	call := s.service.Channels.List([]string{"id", "snippet", "status", "brandingSettings"}).Id(id)
	response, err := call.Do()
	if err != nil {
		return nil, errors.Wrap(err, "unable to get query response")
	}
	if len(response.Items) == 0 {
		return nil, errors.New("channel not found")
	}

	channel := response.Items[0]

	var customURL string
	if channel.Snippet.CustomUrl != "" {
		customURL = channel.Snippet.CustomUrl[1:]
	} else {
		customURL = slug.Make(channel.Snippet.Title)
		customURL = strings.Replace(customURL, "-", "_", -1)
	}

	if customURL == "" {
		return nil, errors.New("wrong channel")
	}

	var bannerURL string
	if channel.BrandingSettings != nil && channel.BrandingSettings.Image != nil {
		bannerURL = channel.BrandingSettings.Image.BannerExternalUrl
	}

	return &types.Channel{
		ID:          channel.Id,
		Title:       channel.Snippet.Title,
		Image:       channel.Snippet.Thumbnails.Default.Url,
		CustomURL:   customURL,
		Description: channel.Snippet.Description,
		BannerURL:   bannerURL,
	}, nil
}

func (s *Service) GetChannelVideos(channelID string) ([]types.Video, error) {
	rsp, err := s.service.Search.List([]string{"id", "snippet"}).ChannelId(channelID).Order("date").MaxResults(10).Do()
	if err != nil {
		return nil, errors.Wrap(err, "unable to search")
	}

	var videos []types.Video
	for _, r := range rsp.Items {
		if r.Id.Kind == "youtube#video" {
			tm, err := time.Parse("2006-01-02T15:04:05Z", r.Snippet.PublishedAt)
			if err != nil {
				return nil, errors.Wrap(err, "unable to parse video published data")
			}

			thumb := r.Snippet.Thumbnails.Default.Url
			if r.Snippet.Thumbnails.High != nil {
				thumb = r.Snippet.Thumbnails.High.Url
			}

			videos = append(videos, types.Video{
				ID:          r.Id.VideoId,
				Title:       r.Snippet.Title,
				Description: r.Snippet.Description,
				Thumbnail:   thumb,
				PublishedAt: tm,
			})
		}
	}

	return videos, nil
}
