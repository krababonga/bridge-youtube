package queue

import (
	"bridge-youtube/types"
	"context"
	"fmt"

	"github.com/pkg/errors"
	amqp "github.com/rabbitmq/amqp091-go"
)

type Config struct {
	RQHost       string
	RQPort       string
	RQUser       string
	RQPassword   string
	ConsumerName string
}

type RabbitMQ struct {
	conn *amqp.Connection
	cnf  *Config
}

func Make(cnf *Config) (*RabbitMQ, error) {
	conn, err := amqp.Dial(fmt.Sprintf("amqp://%s:%s@%s:%s/", cnf.RQUser, cnf.RQPassword,
		cnf.RQHost, cnf.RQPort))
	if err != nil {
		return nil, errors.Wrap(err, "unable to init rq")
	}

	return &RabbitMQ{
		conn: conn,
		cnf:  cnf,
	}, nil
}

func (r *RabbitMQ) Send(ctx context.Context, queueName string, data string) error {
	ch, err := r.conn.Channel()
	if err != nil {
		return errors.Wrap(err, "unable to init rabbit channel")
	}
	defer ch.Close()

	q, err := ch.QueueDeclare(
		queueName, // name
		false,     // durable
		false,     // delete when unused
		false,     // exclusive
		false,     // no-wait
		nil,       // arguments
	)

	if err != nil {
		return errors.Wrap(err, "unable to declare queue")
	}

	err = ch.PublishWithContext(ctx,
		"",     // exchange
		q.Name, // routing key
		false,  // mandatory
		false,  // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(data),
		})

	if err != nil {
		return errors.Wrap(err, "unable to send message to rq")
	}

	return nil
}

func (r *RabbitMQ) Consume(queueName string) (<-chan types.Message, error) {
	ch, err := r.conn.Channel()
	if err != nil {
		return nil, errors.Wrap(err, "unable to init rabbit channel")
	}
	//defer ch.Close()

	q, err := ch.QueueDeclare(
		queueName, // name
		false,     // durable
		false,     // delete when unused
		false,     // exclusive
		false,     // no-wait
		nil,       // arguments
	)

	if err != nil {
		return nil, errors.Wrap(err, "unable to declare queue")
	}

	messages := make(chan types.Message, 10)

	delivery, err := ch.Consume(
		q.Name,             // queue
		r.cnf.ConsumerName, // consumer
		true,               // auto-ack
		false,              // exclusive
		false,              // no-local
		false,              // no-wait
		nil,                // args
	)

	if err != nil {
		return nil, errors.Wrap(err, "unable to start consuming")
	}

	go func() {
		for d := range delivery {
			messages <- types.Message{Body: string(d.Body)}
		}
	}()

	return messages, nil
}
