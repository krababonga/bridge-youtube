package cron

import (
	"bridge-youtube/db"
	"bridge-youtube/types"
	"context"
	"time"

	"github.com/pkg/errors"
)

type FediClient interface {
	AccountFollowers(account *types.FediAccount) ([]types.FediFollower, error)
}

type Queue interface {
	Send(ctx context.Context, queueName string, data string) error
}

type DBConnector interface {
	GetUsers() ([]db.User, error)
}

type Command struct {
	dbConn    DBConnector
	queue     Queue
	fedi      FediClient
	queueName string
}

func Make(dbConn DBConnector, queue Queue, fedi FediClient, queueName string) *Command {
	return &Command{
		fedi:      fedi,
		queue:     queue,
		dbConn:    dbConn,
		queueName: queueName,
	}
}

func (c *Command) Do(ctx context.Context) error {
	users, err := c.dbConn.GetUsers()
	if err != nil {
		return errors.Wrap(err, "unable to get users")
	}

	for _, u := range users {
		followers, err := c.fedi.AccountFollowers(&types.FediAccount{
			ID:       u.FediID,
			Password: u.Password,
			Email:    u.Email,
		})

		if err != nil {
			return errors.Wrap(err, "unable to get account followers")
		}

		if len(followers) > 0 {
			sendCtx, cancel := context.WithTimeout(ctx, time.Second*3)
			if err := c.queue.Send(sendCtx, c.queueName, u.Username); err != nil {
				cancel()
				return errors.Wrap(err, "unable to send username to queue")
			}
			cancel()
		}
	}

	return nil
}
