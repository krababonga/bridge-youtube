package db

import (
	"github.com/glebarez/sqlite"
	"github.com/pkg/errors"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type Connector struct {
	conn *gorm.DB
}

const (
	dbTypeSqlite   = "sqlite"
	dbTypePostgres = "postgres"
)

func (c *Connector) GetConn() *gorm.DB {
	return c.conn
}

func Make(dbType string, dsn string) (*Connector, error) {
	var (
		conn    *gorm.DB
		openErr error
	)
	if dbType == dbTypeSqlite {
		conn, openErr = gorm.Open(sqlite.Open(dsn))
		if openErr != nil {
			return nil, errors.Wrap(openErr, "unable to create sqlite conn")
		}
	} else if dbType == dbTypePostgres {
		conn, openErr = gorm.Open(postgres.Open(dsn))
		if openErr != nil {
			return nil, errors.Wrap(openErr, "unable to create postgres conn")
		}
	} else {
		return nil, errors.New("unknown db type")
	}

	if err := conn.AutoMigrate(&User{}, &Video{}, &SearchResults{}); err != nil {
		return nil, errors.Wrap(err, "unable to auo migrate")
	}

	return &Connector{
		conn: conn,
	}, nil
}

func (c *Connector) GetUserBySearch(search string) (*User, error) {
	var (
		u User
		s SearchResults
	)

	if err := c.conn.First(&s, "request = ?", search).Error; err != nil {
		return nil, errors.Wrap(err, "unable to find  search result")
	}

	if err := c.conn.First(&u, "id = ?", s.UserID).Error; err != nil {
		return nil, errors.Wrap(err, "unable to get user")
	}

	return &u, nil
}

func (c *Connector) GetUser(username string) (*User, error) {
	var u User
	if err := c.conn.First(&u, "username = ?", username).Error; err != nil {
		return nil, errors.Wrap(err, "unable to get user")
	}

	return &u, nil
}

func (c *Connector) CreateUser(u *User) error {
	if err := c.conn.Create(u).Error; err != nil {
		return errors.Wrap(err, "unable to create user")
	}

	return nil
}

func (c *Connector) UpdateUser(u *User) error {
	if err := c.conn.Save(u).Error; err != nil {
		return errors.Wrap(err, "unable to save user")
	}

	return nil
}

func (c *Connector) SaveSearchResult(text string, userID uint) error {
	if err := c.conn.Create(&SearchResults{
		UserID:  userID,
		Request: text,
	}).Error; err != nil {
		return errors.Wrap(err, "unable to save search result")
	}

	return nil
}

func (c *Connector) GetVideo(id string) (*Video, error) {
	var v Video
	if err := c.conn.First(&v, "id = ?", id).Error; err != nil {
		return nil, errors.Wrap(err, "unable to get video")
	}

	return &v, nil
}

func (c *Connector) SaveVideo(v *Video) error {
	if err := c.conn.Create(v).Error; err != nil {
		return errors.Wrap(err, "unable to create video")
	}

	return nil
}

func (c *Connector) GetUsers() ([]User, error) {
	var usrs []User
	if err := c.conn.Find(&usrs).Error; err != nil {
		return nil, errors.Wrap(err, "unable to load users")
	}

	return usrs, nil
}
