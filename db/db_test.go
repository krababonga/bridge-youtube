package db

import (
	"testing"

	"github.com/pkg/errors"
	"gorm.io/gorm"
)

func TestConnector_CreateUser(t *testing.T) {
	conn, err := Make(dbTypeSqlite, "/tmp/bridge.sqlite")
	if err != nil {
		t.Fatal(err)
	}

	_, err = conn.GetUser("__user_test")
	if errors.Cause(err) != gorm.ErrRecordNotFound {
		t.Fatal(err)
	}

	if err := conn.CreateUser(&User{
		Username:    "_test",
		AccessToken: "_gest",
		Password:    "123",
		Email:       "dwqdqw@sadsa.ss",
	}); err != nil {
		t.Fatal(err)
	}
}
