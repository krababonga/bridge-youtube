package db

import (
	"fmt"
	"time"

	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	Username    string
	Password    string
	Email       string
	AccessToken string
	YtChannelID string
	DisplayName string
	Description string
	FediID      string
	LastUpdate  *time.Time
}

type Video struct {
	gorm.Model
	ID            string
	Title         string
	Description   string
	Thumbnail     string
	ChannelID     string
	PublishedAt   time.Time
	FediID        string
	ThumbnailSize int
}

type SearchResults struct {
	gorm.Model
	Request string
	UserID  uint
}

func (v Video) URL() string {
	return fmt.Sprintf("https://www.youtube.com/watch?v=%s", v.ID)
}
