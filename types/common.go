package types

import (
	"io"
	"net/http"

	"github.com/pkg/errors"
)

func DownloadImage(path string) ([]byte, error) {
	rsp, err := http.Get(path)
	if err != nil {
		return nil, errors.Wrap(err, "unable to download image")
	}

	if rsp.StatusCode >= 300 {
		return nil, errors.Errorf("code: %d", rsp.StatusCode)
	}

	bts, err := io.ReadAll(rsp.Body)
	if err != nil {
		return nil, errors.Wrap(err, "unable to read body")
	}

	return bts, nil
}
