package types

type FediAccount struct {
	ID          string
	Username    string
	Email       string
	Password    string
	AccessToken string
	Locked      bool
	Sensitive   bool
	DisplayName string
	Avatar      []byte
	Banner      []byte
	Description string
	IsBot       bool
}

type FediFollower struct {
	ID       string
	Username string
}
