package types

import "context"

type Queue interface {
	Consume(queueName string) (<-chan Message, error)
	Send(ctx context.Context, queueName string, data string) error
}

type Message struct {
	Body string
}
