package types

import (
	"fmt"
	"time"
)

type Channel struct {
	ID          string
	Title       string
	Image       string
	CustomURL   string
	Description string
	BannerURL   string
}

type Video struct {
	ID          string
	Title       string
	Description string
	Thumbnail   string
	PublishedAt time.Time
}

func (v Video) URL() string {
	return fmt.Sprintf("https://www.youtube.com/watch?v=%s", v.ID)
}
